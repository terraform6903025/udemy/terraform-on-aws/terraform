# Resource Block
# Resource-1: Create VPC
resource "aws_vpc" "vpc-dev" {
  provider = myaws
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "vpc-dev"
  }
}

# Resource-2: Create Subnets
resource "aws_subnet" "vpc-dev-public-subnet-1" {
  provider = myaws
  vpc_id = aws_vpc.vpc-dev.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "ap-south-1a"
  map_public_ip_on_launch = true
  tags = {
    Name = "${resource.aws_vpc.vpc-dev.tags["Name"]}-public-sn"
  }
}

# Resource-3: Internet Gateway
resource "aws_internet_gateway" "vpc-dev-igw" {
  provider = myaws
  vpc_id = aws_vpc.vpc-dev.id
  tags = {
    Name = "${resource.aws_vpc.vpc-dev.tags["Name"]}-igw"
  }
}

# Resource-4: Create Route Table
resource "aws_route_table" "vpc-dev-public-route-table" {
  provider = myaws
  vpc_id = aws_vpc.vpc-dev.id
  tags = {
    Name = "${resource.aws_vpc.vpc-dev.tags["Name"]}-public-rtb"
  }
}

# Resource-5: Create Route in Route Table for Internet Access
resource "aws_route" "vpc-dev-public-route" {
  provider = myaws
  route_table_id = aws_route_table.vpc-dev-public-route-table.id 
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.vpc-dev-igw.id 
}
# Resource-6: Associate the Route Table with the Subnet
resource "aws_route_table_association" "vpc-dev-public-route-table-associate" {
  provider = myaws
  route_table_id = aws_route_table.vpc-dev-public-route-table.id 
  subnet_id = aws_subnet.vpc-dev-public-subnet-1.id
}
