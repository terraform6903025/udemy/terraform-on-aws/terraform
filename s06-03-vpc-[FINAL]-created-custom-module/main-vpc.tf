module "my-vpc-module" {
    source = "./modules/vpcmodule"
    # variable to override - start
    environment = var.environment # default = "dev"  (as per config in vpc-variables) 
    business_divsion = var.business_divsion # default = "SAP" (as per config in vpc-variables) 
    vpc_availability_zones = [ "ap-south-1a", "ap-south-1b" ] # default = ["some-garbage-az-01", "some-garbage-az-02"] (as per config in vpc.auto.tfvars) 
    # variable to override - end  
}