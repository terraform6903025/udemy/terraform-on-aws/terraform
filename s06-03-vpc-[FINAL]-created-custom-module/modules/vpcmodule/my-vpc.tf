# Create VPC Terraform Module
module "my_vpc" {  
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.0.0"

  # VPC Basic Details
  name = "${local.name}-${var.vpc_name}"
  cidr = var.vpc_cidr_block   
  azs                 = var.vpc_availability_zones
  private_subnets     = var.vpc_private_subnets
  public_subnets      = var.vpc_public_subnets

  
  # Database Subnets
  
  create_database_subnet_group = var.vpc_create_database_subnet_group
  create_database_subnet_route_table= var.vpc_create_database_subnet_route_table
  database_subnets    = var.vpc_database_subnets

  #create_database_nat_gateway_route = true
  #create_database_internet_gateway_route = true

  # NAT Gateways - Outbound Communication
  enable_nat_gateway = true
  single_nat_gateway = true

  # VPC DNS Parameters
  enable_dns_hostnames = true
  enable_dns_support = true

  public_subnet_tags = {
    Type = "public-subnets"
    # Name = "ksc-public-subnets" # un-comment to override common name i.e module.my_vpc.name
  }

  private_subnet_tags = {
    Type = "private-subnets"
    # Name = "ksc-private-subnets" # un-comment to override common name i.e module.my_vpc.name
  }

  database_subnet_tags = {
    Type = "database-subnets"
    # Name = "ksc-database-subnets" # un-comment to override common name i.e module.my_vpc.name
  }

  tags = local.common_tags
  vpc_tags = local.common_tags
}