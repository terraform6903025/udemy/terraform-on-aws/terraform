# Input Variables
# AWS Region
variable "aws_region" {
  description = "Region in which AWS Resources to be created"
  type = string
  # default = "ap-south-1" # the value to this variable is assigned from terraform.tfvars  
}

# Environment Variable
variable "environment" {
  description = "Environment Variable used as a prefix"
  type = string
  # default = "dev" # the value to this variable is assigned from terraform.tfvars 
}
# Business Division
variable "business_divsion" {
  description = "Business Division in the large organization this Infrastructure belongs"
  type = string
  # default = "SAP" # the value to this variable is assigned from terraform.tfvars 
}