output "aws_region" {
    description = "aws_region output"
    value = var.aws_region
}

output "vpc_name" {
    description = "vpc_name output"
    value = module.my-vpc-module.vpc_name
}