# resource
resource "aws_instance" "myec2Instance" {
    provider = myaws
    ami = "ami-057752b3f1d6c4d6c"
    instance_type = "t2.micro"
    user_data = file("${path.module}/app1-install.sh")
    tags = {
      "Name":"ec2-demo-tf"
    }
}