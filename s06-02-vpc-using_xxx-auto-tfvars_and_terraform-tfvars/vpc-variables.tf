# VPC Input Variables

# VPC Name
variable "vpc_name" {
  description = "VPC Name"
  type = string 
   default = "myvpc" # the value to this variable is overridden from terraform.tfvars
}

# VPC CIDR Block
variable "vpc_cidr_block" {
  description = "VPC CIDR Block"
  type = string 
  default = "199.0.0.0/16" # the value to this variable is overridden from terraform.tfvars
}

# VPC Availability Zones
variable "vpc_availability_zones" {
  description = "VPC Availability Zones"
  type = list(string)
  default = ["garbage-az-1", "garbaze-az-2"] # the value to this variable is overridden from terraform.tfvars
}

# VPC Public Subnets
variable "vpc_public_subnets" {
  description = "VPC Public Subnets"
  type = list(string)
  default = ["199.0.101.0/24", "199.0.102.0/24"] # the value to this variable is overridden from terraform.tfvars
}

# VPC Private Subnets
variable "vpc_private_subnets" {
  description = "VPC Private Subnets"
  type = list(string)
  default = ["199.0.1.0/24", "199.0.2.0/24"]  # the value to this variable is overridden from terraform.tfvars
}

# VPC Database Subnets
variable "vpc_database_subnets" {
  description = "VPC Database Subnets"
  type = list(string)
  default = ["199.0.151.0/24", "199.0.152.0/24"] # the value to this variable is overridden from terraform.tfvars
}

# VPC Create Database Subnet Group (True / False)
variable "vpc_create_database_subnet_group" {
  description = "VPC Create Database Subnet Group"
  type = bool
  default = false # the value to this variable is overridden from terraform.tfvars
}

# VPC Create Database Subnet Route Table (True or False)
variable "vpc_create_database_subnet_route_table" {
  description = "VPC Create Database Subnet Route Table"
  type = bool
  default = false # the value to this variable is overridden from terraform.tfvars   
}