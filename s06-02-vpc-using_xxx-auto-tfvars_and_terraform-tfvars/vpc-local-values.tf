locals {
  owners = var.business_divsion
  env = var.environment
  # name = "${var.business_divsion}-${var.environment}" # valid syntax
  name = "${local.owners}-${local.env}"
  common_tags = {
    owners = local.owners
    env = local.env
  }
}