# AWS region
variable "aws_region" {
    description = "aws region"
    type = string
    default = "ap-south-1"
}