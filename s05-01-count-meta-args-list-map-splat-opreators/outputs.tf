# COMMENTED SINCE WE HAVE USED count META-ARGS IN aws_instance.myec2Instance.public_ip
/*
# ec2 instance public_ip
output "instance_public_ip" {
    description = "instance_public_ip desc"
    value = aws_instance.myec2Instance.public_ip
}

# ec2 instance public_dns
output "instance_public_dns" {
    description = "instance_public_dns desc"
    value = aws_instance.myec2Instance.public_dns
}
*/

# Output - For loop With List
output "for_output_loop" {
  description = "for loop with list"
  /*
  the below statement read as print public_dns for all aws_instance.myec2Instance 
  */
  value = [for x in aws_instance.myec2Instance : x.public_dns]
}

# Output - For loop With map - Way 1
output "for_output_map_1" {
  description = "for loop with map way 1"
  /*
  the below statement read as print public_dns for all aws_instance.myec2Instance as map with the key as public_dns
  */
  value = {
    for x in aws_instance.myec2Instance : x.public_dns => x.public_dns 
  }
}

# Output - For loop With map - Way 2 - Advanced
output "for_output_map_2" {
  description = "for loop with map way 2 advanced"
  /*
  the below statement read as print public_dns for all aws_instance.myec2Instance as map with the key as count.index
  */
  value = {
    for ci, x in aws_instance.myec2Instance : ci => x.public_dns 
  }
}

# Output - legacy splat operator - RETURNS THE LIST
/*
SOON TO BE DEPRECATED use latest(generic) splat operator
*/
output "legacy_splat_instance" {
  description = "legacy_splat_instance(SOON TO BE DEPRECATED) use latest(generic) splat operator"
  /*
  TBD
  */
  value = aws_instance.myec2Instance.*.public_dns
}

# Output - latest(generic) splat operator - RETURNS THE LIST
output "latest_generic_splat_instance" {
  description = "latest_generic_splat_instance"
  /*
  TBD
  */
  value = aws_instance.myec2Instance[*].public_dns
}