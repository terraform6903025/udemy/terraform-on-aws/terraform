# terraform block
terraform {
  required_version = "~> 1.4.6"
  required_providers {
    myaws = {
      source = "hashicorp/aws"
      version = "~> 5.3.0"
    }
  }
}

# provider block
provider "myaws" {
    profile = "default"
    region = var.aws_region # from variables
}
