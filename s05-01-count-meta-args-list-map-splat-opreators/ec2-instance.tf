# resource
resource "aws_instance" "myec2Instance" {
  # meta args - start
  count = 3
  # meta args - end 
  provider = myaws # from  main terraform block
  ami = data.aws_ami.myAmzLinux2.id # from ami-datasource 
  # WORKING
  instance_type = var.instance_type # from variables
  # WORKING
  # instance_type = var.instance_type_list[0] # for list # from variables
  # WORKING
  # instance_type = var.instance_type_map["prod"] # for map # from variables
  user_data = file("${path.module}/app1-install.sh") 
  key_name = var.instance_keypair # from variables
  vpc_security_group_ids = [
    aws_security_group.allow_ssh.id, # from security-groups
    aws_security_group.allow_web.id # from security-groups
  ]
  tags = {
    "Name" : "count-demo-tf-${count.index}" # count.index depends on count meta-args, if count = 2, then count.index will have 2 value as 0,1
  }
}