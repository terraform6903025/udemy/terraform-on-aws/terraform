# Input variables

# AWS region
variable "aws_region" {
  description = "Region in which AWS resources to be createddddd"
  type = string
  default = "ap-south-1"
}

# AWS instance type
variable "instance_type" {
  description = "instance type of ec2 instancessssssssssssss"
  type = string
  default = "t2.micro"
}

# AWS ec2 key pair
variable "instance_keypair" {
  description = "keypair associated to ec2 instancessssssssssssss"
  type = string
  default = "terraform-ec2" # this value taken from AWS console -> ec2 key pairs
}

variable "instance_type_list" {
  description = "list"
  type = list(string)
  default = [ "t2.micro", "t2.small" ]
}

variable "instance_type_map" {
  description = "map"
  type = map(string)
  default = {
    "dev" = "t2.micro"
    "qa" = "t2.small"
    "prod" = "t2.medium"
  }
}