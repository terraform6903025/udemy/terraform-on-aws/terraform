data "aws_availability_zones" "my_azones" {
  provider = myaws
  filter {
    name   = "opt-in-status"
    values = ["opt-in-not-required"]
  }
}