# resource
resource "aws_instance" "myec2Instance" {
  # meta args - start
  # count = 3 commented for s05-02
  # meta args - end 
  provider = myaws # from  main terraform block
  ami = data.aws_ami.myAmzLinux2.id # from ami-datasource 
  # WORKING
  instance_type = var.instance_type # from variables
  # WORKING
  # instance_type = var.instance_type_list[0] # for list # from variables
  # WORKING
  # instance_type = var.instance_type_map["prod"] # for map # from variables
  user_data = file("${path.module}/app1-install.sh") 
  key_name = var.instance_keypair # from variables
  vpc_security_group_ids = [
    aws_security_group.allow_ssh.id, # from security-groups
    aws_security_group.allow_web.id # from security-groups
  ]
  # s05-02 - start
  # s05-03 - starts
  # for_each = toset(data.aws_availability_zones.my_azones.names) # from availability-zones-datasource
  # refer - get-instancetype-supported-per-az-in-a-region
  for_each = toset( keys( { for az, details in data.aws_ec2_instance_type_offerings.my_ins_type :
                            az => details.instance_types if length(details.instance_types) != 0 }))  
  # s05-03 - ends
  availability_zone = each.key # each.key for set returns value, but for map each.key returns key and each.value return value 
  
  tags = {
    "Name" : "for-each-demo-tf-${each.value}" # for set each.value = each.key, but for map each.key returns key and each.value return value
  }
  # s05-02 - end
}