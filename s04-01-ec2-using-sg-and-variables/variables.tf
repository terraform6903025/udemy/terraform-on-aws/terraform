# Input variables

# AWS region
variable "aws_region" {
  description = "Region in which AWS resources to be createddddd"
  type = string
  default = "ap-south-1"
}

# AWS instance type
variable "instance_type" {
  description = "instance type of ec2 instancessssssssssssss"
  type = string
  default = "t2.micro"
}

# AWS ec2 key pair
variable "instance_keypair" {
  description = "keypair associated to ec2 instancessssssssssssss"
  type = string
  default = "terraform-ec2" # this value taken from AWS console -> ec2 key pairs
}

