# resource

resource "aws_instance" "myec2Instance" {
    provider = myaws # from  main terraform block
    ami = data.aws_ami.myAmzLinux2.id # from ami-datasource 
    instance_type = var.instance_type # from variables
    user_data = file("${path.module}/app1-install.sh")
    key_name = var.instance_keypair # from variables
    vpc_security_group_ids = [
      aws_security_group.allow_ssh.id, # from security-groups
      aws_security_group.allow_web.id # from security-groups
    ]
    tags = {
      "Name" : "ec2-demo-tf"
    }
}