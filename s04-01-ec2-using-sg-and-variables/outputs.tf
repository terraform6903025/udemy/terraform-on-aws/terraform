# ec2 instance public_ip
output "instance_public_ip" {
    description = "instance_public_ip desc"
    value = aws_instance.myec2Instance.public_ip
}

# ec2 instance public_dns
output "instance_public_dns" {
    description = "instance_public_dns desc"
    value = aws_instance.myec2Instance.public_dns
}